from setuptools import setup, Extension


setup(
    name='insert_powers',
    version='1.0',
    description='Python Package with insert_powers Extension',
    ext_modules=[
        Extension(
            'insert_powers',
            sources=['insertpowersmodule.cpp'],
            py_limited_api=True)
    ],
)
