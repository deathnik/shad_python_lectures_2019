from random import randint, random
from typing import Iterable, TypeVar, List

T = TypeVar('T')


def _internal_quick_sort(elements: List[T], begin_index: int, end_index: int) -> None:
    """Sort list inplace using QuickSort"""
    if end_index - begin_index <= 1:
        return

    pivot = elements[randint(begin_index, end_index - 1)]

    second_index = begin_index

    def move_right_start(current_index):
        elements[second_index], elements[current_index] = elements[current_index], elements[second_index]

    for index in range(begin_index, end_index):
        if elements[index] < pivot:
            move_right_start(index)
            second_index += 1
        elif elements[index] == pivot and random() < 0.5:
            move_right_start(index)
            second_index += 1

    _internal_quick_sort(elements, begin_index, second_index)
    _internal_quick_sort(elements, second_index, end_index)


def quick_sort(elements: Iterable[T]) -> List[T]:
    """Sort iterable sequence using QuickSort algorithm"""
    elements = list(elements)
    _internal_quick_sort(elements, 0, len(elements))

    return elements


if __name__ == '__main__':
    assert quick_sort([1, 2]) == [1, 2]
    assert quick_sort([2, 1]) == [1, 2]

    assert quick_sort([2, 2, 2, 1]) == [1, 2, 2, 2]

    assert quick_sort(range(5, 0, -1)) == [1, 2, 3, 4, 5]

    assert quick_sort([]) == []

    assert quick_sort([2, 5, 3, 4, 1]) == [1, 2, 3, 4, 5]
